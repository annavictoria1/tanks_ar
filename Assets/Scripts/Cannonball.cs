using UnityEngine;

public class Cannonball : MonoBehaviour
{
    [SerializeField] private float explosionRadius = 5f;
    [SerializeField] private float maxDamage = 50f;
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private AudioClip explosionSound;

    private void OnCollisionEnter(Collision collision)
    {
        Explode();
        PlayExplosionSound();
        Destroy(gameObject);
    }

    private void Explode()
    {
        if (explosionPrefab != null)
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);
        }

        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider hit in colliders)
        {
            TankHealth tankHealth = hit.GetComponent<TankHealth>();
            if (tankHealth != null)
            {
                float distance = Vector3.Distance(transform.position, hit.transform.position);
                float damage = CalculateDamage(distance);
                tankHealth.TakeDamage(damage);
            }
        }
    }

    private float CalculateDamage(float distance)
    {
        float normalizedDistance = Mathf.Clamp01(distance / explosionRadius);
        return Mathf.Lerp(maxDamage, 0, normalizedDistance);
    }

    private void PlayExplosionSound()
    {
        GameObject soundObject = new GameObject("ExplosionSound");
        soundObject.transform.position = transform.position;
        AudioSource audioSource = soundObject.AddComponent<AudioSource>();
        audioSource.clip = explosionSound;
        audioSource.Play();
        Destroy(soundObject, explosionSound.length);
    }
}
