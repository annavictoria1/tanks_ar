using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MantletMove : MonoBehaviour
{
    [SerializeField] private Transform mantlet;
    [SerializeField] private float rotationSpeed, rotateMin, rotateMax;
    [SerializeField] private StickController stickController;
    private void Awake()
    {
        if (stickController == null)
        {
            Debug.Log("No stick controller");
        }
        else
        {
            stickController.StickChanged += MoveStick_StickChanged;
        }
    }
    private Vector2 stickPos = Vector2.zero;
    private void MoveStick_StickChanged(object sender, StickEventArgs e)
    {
        stickPos = e.Position;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void LateUpdate()
    {
        float h = stickPos.x * rotationSpeed; // Input.GetAxis("Horizontal");
        float v = stickPos.y * rotationSpeed; // Input.GetAxis("Vertical");

        // Get current local rotation
        Vector3 currentRotation = mantlet.localEulerAngles;

        // Calculate new rotation with clamping
        float newRotationX = currentRotation.x + h;
        newRotationX = ClampAngle(newRotationX, rotateMin, rotateMax);

        // Apply the clamped rotation
        mantlet.localEulerAngles = new Vector3(newRotationX, currentRotation.y, currentRotation.z);
    }
    // Helper method to clamp angles correctly
    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < 0f)
            angle += 360f;
        if (angle > 180f)
            angle -= 360f;

        return Mathf.Clamp(angle, min, max);
    }
}
