using System;
using UnityEngine;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour
{
    [SerializeField] private float maxHealth = 100f;
    [SerializeField] private Image healthBarImage;
    [SerializeField] private AudioClip destroyedSound;
    [SerializeField] private GameObject explosionPrefab;
    private float currentHealth;
    public event Action<TankHealth> OnTankDestroyed;

    private void Awake()
    {
        currentHealth = maxHealth;
        UpdateHealthBar();
    }

    public void TakeDamage(float amount)
    {
        currentHealth -= amount;
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        UpdateHealthBar();

        if (currentHealth <= 0)
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);
            PlayDestroySound();
            OnTankDestroyed?.Invoke(this);
            Destroy(gameObject);
        }
    }

    private void UpdateHealthBar()
    {
        if (healthBarImage != null)
        {
            healthBarImage.fillAmount = currentHealth / maxHealth;
        }
    }

    private void PlayDestroySound()
    {
        GameObject soundObject = new GameObject("DestroySound");
        soundObject.transform.position = transform.position;
        AudioSource audioSource = soundObject.AddComponent<AudioSource>();
        audioSource.clip = destroyedSound;
        audioSource.Play();
        Destroy(soundObject, destroyedSound.length);
    }
}
