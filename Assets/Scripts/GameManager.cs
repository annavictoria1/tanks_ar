using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private bool isGreyTankTurn = false;
    [SerializeField] private PlayerController greenController, greyController;
    [SerializeField] private MantletMove greenMantlet, greyMantlet;
    [SerializeField] private TankHealth greyTankHealth;
    [SerializeField] private TankHealth greenTankHealth;
    [SerializeField] private GameObject shootButtonGrey, shootButtonGreen;
    [SerializeField] private AudioClip playFanfare;
    [SerializeField] private TextMeshProUGUI turnTxt;
    [SerializeField] private GameObject greenIcon, greyIcon;

    // Start is called before the first frame update
    void Start()
    {
        greyTankHealth.OnTankDestroyed += HandleTankDestroyed;
        greenTankHealth.OnTankDestroyed += HandleTankDestroyed;
        UpdateTurn();
    }

    private void UpdateTurn()
    {
        if (isGreyTankTurn)
        {
            greyMantlet.enabled = true;
            greyController.enabled = true;
            shootButtonGrey.SetActive(true);
            greenMantlet.enabled = false;
            greenController.enabled = false;
            shootButtonGreen.SetActive(false);
            turnTxt.text = "Grey Tank's Turn";
            turnTxt.color = Color.gray;
            greyIcon.SetActive(true);
            greenIcon.SetActive(false);
        }
        else
        {
            greyMantlet.enabled = false;
            greyController.enabled = false;
            shootButtonGrey.SetActive(false);
            greenMantlet.enabled = true;
            greenController.enabled = true;
            shootButtonGreen.SetActive(true);
            turnTxt.text = "Green Tank's Turn";
            turnTxt.color = Color.green;
            greyIcon.SetActive(false);
            greenIcon.SetActive(true);
        }
    }

    public void SwitchTurn()
    {
        isGreyTankTurn = !isGreyTankTurn;
        UpdateTurn();
    }

    private void HandleTankDestroyed(TankHealth destroyedTank)
    {
        if (destroyedTank == greyTankHealth)
        {
            Debug.Log("Green tank wins!");
            greyIcon.SetActive(false);
            greenIcon.SetActive(true);
            turnTxt.text = "Green Tank Wins!";
            turnTxt.color = Color.green;
        }
        else if (destroyedTank == greenTankHealth)
        {
            Debug.Log("Grey tank wins!");
            greyIcon.SetActive(true);
            greenIcon.SetActive(false);
            turnTxt.text = "Grey Tank Wins!";
            turnTxt.color = Color.gray;
        }
        greyMantlet.enabled = false;
        greyController.enabled = false;
        greenMantlet.enabled = false;
        greenController.enabled = false;
        shootButtonGreen.SetActive(false);
        shootButtonGrey.SetActive(false);
        GetComponent<AudioSource>().PlayOneShot(playFanfare);
    }
}
