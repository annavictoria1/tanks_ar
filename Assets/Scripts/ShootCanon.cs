using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootCanon : MonoBehaviour
{
    [SerializeField] private Transform mantlet;
    [SerializeField] private GameObject cannonballPrefab;
    [SerializeField] private Transform shootingPoint;
    [SerializeField] private float initialShootingForce = 10f;
    [SerializeField] private float maxShootingForce = 100f;
    [SerializeField] private float forceIncreaseRate = 10f;
    [SerializeField] private Image forceIndicatorImage;
    [SerializeField] private GameObject shootingForceUI;
    [SerializeField] private GameManager gameManager;

    private float currentShootingForce;
    private bool isCharging;

    // Update is called once per frame
    void Update()
    {
        if (isCharging)
        {
            currentShootingForce += forceIncreaseRate * Time.deltaTime;
            currentShootingForce = Mathf.Clamp(currentShootingForce, initialShootingForce, maxShootingForce);
            UpdateForceIndicator();
        }
        else
        {
            shootingForceUI.SetActive(false);
        }
    }

    public void StartCharging()
    {
        isCharging = true;
        currentShootingForce = initialShootingForce;
        UpdateForceIndicator();
    }

    public void Shoot()
    {
        if (isCharging)
        {
            GameObject cannonball = Instantiate(cannonballPrefab, shootingPoint.position, shootingPoint.rotation);
            Rigidbody rb = cannonball.GetComponent<Rigidbody>();
            rb.AddForce(mantlet.forward * currentShootingForce, ForceMode.Impulse);
            isCharging = false;
            currentShootingForce = initialShootingForce;
            UpdateForceIndicator();
            gameManager.SwitchTurn();
        }
    }

    private void UpdateForceIndicator()
    {
        shootingForceUI.SetActive(true);
        if (forceIndicatorImage != null)
        {
            float fillAmount = (currentShootingForce - initialShootingForce) / (maxShootingForce - initialShootingForce);
            forceIndicatorImage.fillAmount = fillAmount;
        }
    }
}
